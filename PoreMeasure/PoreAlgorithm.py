from PIL import Image
import datetime
import numpy
import sys
from tqdm import tqdm

progressBarLen = 20

def averageDict(dictionary):
    summ = 0
    amount = 0
    for element in dictionary:
        summ += element * dictionary[element]
        amount += dictionary[element]
    return summ / amount
def summOfNeighbours(matrix, row, colomn, delta):
    sum = 0
    for elementRow in range(max(0, row - delta), min(matrix.shape[0], row + delta)):
        for elementColomn in range(max(0, colomn - delta + 1), min(matrix.shape[1], colomn + delta + 1)):
            sum += matrix[elementRow][elementColomn]
    return sum

def blur(matrix, delta=[0]):
    delta = int(delta[0])
    blured = numpy.empty(matrix.shape, int)
    for row in range(0, matrix.shape[0]):
        for colomn in range(0, matrix.shape[1]):
            blured[row][colomn] = int(summOfNeighbours(matrix, row, colomn, delta) / ((delta * 2 + 1) ** 2))
        percent = float(row) / matrix.shape[0]
        hashes = '#' * int(round(percent * progressBarLen))
        spaces = ' ' * (progressBarLen - len(hashes))
        sys.stdout.write("\rPercent: [{0}] {1}%".format(hashes + spaces, int(round(percent * 100))))
        sys.stdout.flush()
    print('\n')
    return blured
def cut(matrix, parameters=[0,0,0,0]):
    answer = []
    matrix = matrix[int(parameters[1]):]
    if int(parameters[3]):
        matrix = matrix[: -int(parameters[3])]
    for row in matrix:
        row = row[int(parameters[0]):]
        if int(parameters[2]):
            row = row[:-int(parameters[2])]
        answer.append(row)
    return numpy.array(answer)
def selectPores(matrix, percentage=[15]):
    percentage = int(percentage[0])
    l = []
    for i in range(0, 256):
        l.append(0)

    for row in matrix:
        for element in row:
            try:
                l[element] += 1
            except:
                print(element)
                exit(1)
    sum = 0
    for count in range(0, 256):
        sum += l[count]
        if (sum / matrix.size * 100 >= percentage):
            return count
def flaternPic(matrix, min_pore_value):
    flatMatrix = matrix
    for row in range(0, flatMatrix.shape[0]):
        for elem in range(0, flatMatrix.shape[1]):
            flatMatrix[row][elem] = 255 * int(flatMatrix[row][elem] > min_pore_value)
    return flatMatrix

def DFS(matrix, row, elem):
    ans = 0
    stack = [(row, elem)]
    while len(stack):
        row, elem = stack[-1][0], stack[-1][1]
        stack.pop()
        if 0 == matrix[row][elem]:
            ans += 1
        matrix[row][elem] = 255
        if row + 1 < matrix.shape[0] and 0 == matrix[row + 1][elem]:
            stack.append((row + 1, elem))
        if row > 0 and 0 == matrix[row - 1][elem]:
            stack.append((row - 1, elem))
        if elem + 1 < matrix.shape[1] and 0 == matrix[row][elem + 1]:
            stack.append((row, elem + 1))
        if elem > 0 and 0 == matrix[row][elem - 1]:
            stack.append((row, elem - 1))
    return ans
def poreSizes(matrix):
    l = {}
    colored = matrix.copy()
    for row in range(0, matrix.shape[0]):
        for elem in range(0, matrix.shape[1]):
            if 0 == colored[row][elem]:
                size = DFS(colored, row, elem)
                if size not in l.keys():
                    l[size] = 0
                l[size] += 1
    return l

'''
def boudaryDFS(matrix, row, elem):
    ans = [0, 0]
    startRow, startElem = row, elem
    dl, dr, du, dd = 0, 0, 0, 0
    stack = [(row, elem)]
    while len(stack):
        row, elem = stack[-1][0], stack[-1][1]
        stack.pop()
        if 0 == matrix[row][elem]:
            dl = 9
        matrix[row][elem] = 255
        if row + 1 < matrix.shape[0] and 0 == matrix[row + 1][elem]:
            stack.append((row + 1, elem))
        if row > 0 and 0 == matrix[row - 1][elem]:
            stack.append((row - 1, elem))
        if elem + 1 < matrix.shape[1] and 0 == matrix[row][elem + 1]:
            stack.append((row, elem + 1))
        if elem > 0 and 0 == matrix[row][elem - 1]:
            stack.append((row, elem - 1))
    return ans
def poreBoundaries(matrix):
    l = {}
    colored = matrix.copy()
    for row in range(0, matrix.shape[0]):
        for elem in range(0, matrix.shape[1]):
            if 0 == colored[row][elem]:
                size = DFS(colored, row, elem)
                if size not in l.keys():
                    l[size] = 0
                l[size] += 1

    return l
'''